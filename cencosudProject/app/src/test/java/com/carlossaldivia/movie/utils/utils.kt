import com.carlossaldivia.movie.domain.models.home.HomeResponseModel
import com.carlossaldivia.movie.domain.models.home.ResultMovie
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import org.mockito.Mockito
import org.mockito.stubbing.OngoingStubbing

// Test
inline fun <reified T> mock() = Mockito.mock(T::class.java)
inline fun <T> whenever(methodCall: T): OngoingStubbing<T> = Mockito.`when`(methodCall)

//Testing Classes
val genre_ids: List<Long> = arrayListOf(28, 80, 53)
val resulMovie: ResultMovie = ResultMovie(false, "/9yBVqNruk6Ykrwc32qrK2TIE5xw.jpg", genre_ids, 460465, "en", "Mortal Kombat", "Washed-up MMA fighter Cole Young, unaware of his heritage, and hunted by Emperor Shang Tsung's best warrior, Sub-Zero, seeks out and trains with Earth's greatest champions as he prepares to stand against the enemies of Outworld in a high stakes battle for the universe.", 8009.461, "/xGuOF1T3WmPsAcQEQJfnG7Ud9f8.jpg", "2021-04-07", "Mortal Kombat", false, 7.8, 1959)
val results: MutableList<ResultMovie> = mutableListOf(resulMovie)
val homeResponseTest = HomeResponseModel(1, results, 500, 10000)