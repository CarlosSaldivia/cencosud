package com.carlossaldivia.movie.presentation.test


import android.content.Context
import androidx.databinding.ObservableBoolean
import com.carlossaldivia.movie.BuildConfig
import com.carlossaldivia.movie.data.api.RetrofitApi
import com.carlossaldivia.movie.presentation.viewmodel.home.HomeViewModel
import com.carlossaldivia.movie.utils.applyTestSchedulers
import homeResponseTest
import io.reactivex.Flowable
import mock
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import whenever

@RunWith(MockitoJUnitRunner.Silent::class)
class HomeViewModelTest {

    @Mock
    private lateinit var mockContext: Context
    val apiInterface = mock<RetrofitApi>()
    val homeViewModel = mock<HomeViewModel>()
    lateinit var apiKey: String
    lateinit var sortBy: String
    var page: Int = 0

    @Before
    fun setUp() {
        homeViewModel.isLoading = mock<ObservableBoolean>()
        homeViewModel.retrofitApi = apiInterface
        apiKey = BuildConfig.API_KEY
        sortBy = "popularity.desc"
        page = 1
    }

    @Test
    fun testHomeResponseApi() {
        val homeResponse = Flowable.just(homeResponseTest)
        given(apiInterface.getHomeResponse(apiKey, sortBy, page)).willReturn(homeResponse)
        whenever(
            homeViewModel.homeResponseApi(mockContext, apiKey, sortBy, page)
                .applyTestSchedulers()
        ).thenReturn(homeResponse)
    }

}