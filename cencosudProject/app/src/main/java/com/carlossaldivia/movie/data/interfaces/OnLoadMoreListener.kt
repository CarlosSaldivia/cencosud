package com.carlossaldivia.movie.data.interfaces

import com.google.gson.JsonObject

interface OnLoadMoreListener {
    fun onLoadMore(message:String, objectSelected: String)
}