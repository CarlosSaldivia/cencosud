package com.carlossaldivia.movie.presentation.ui.activities

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.carlossaldivia.movie.R
import com.carlossaldivia.movie.utils.Utils
import com.google.android.material.navigation.NavigationView


class MainActivity : AppCompatActivity(){
        private lateinit var drawer: DrawerLayout
        private lateinit var toggle: ActionBarDrawerToggle

        lateinit var navController: NavController
        var validateInternet = false

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_main)

            val toolbar: Toolbar = findViewById(R.id.toolbar_main)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                setSupportActionBar(toolbar)
            }

            drawer = findViewById(R.id.drawer_layout)

            toggle = ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
            drawer.addDrawerListener(toggle)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeButtonEnabled(true)
            toggle.syncState()

            val navigationView: NavigationView = findViewById(R.id.nav_view)
            setupNavigationDrawerContent(navigationView)

            navController = Navigation.findNavController(this, R.id.nav_host_fragment)

            Utils.validatePermission(this, this)
            validateInternet = Utils.validateInternet(this)
            if (!validateInternet) {
                Utils.showAlertDialog(
                    this,
                    "error",
                    "",
                    getString(R.string.error_internet),
                    "Aceptar",
                    "",
                    "HomeFragment_error_internet",
                    null
                )
            }

        }

    private fun setupNavigationDrawerContent(navigationView: NavigationView) {
        navigationView.setNavigationItemSelectedListener(
            NavigationView.OnNavigationItemSelectedListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.nav_home -> {
                        Log.d("GLOBAL", "GLOBAL_RESPONSE_: 100")
                        navController.navigate(R.id.homeFragment)
                        drawer.closeDrawer(GravityCompat.START)
                        return@OnNavigationItemSelectedListener true
                    }
                    R.id.nav_movie -> {
                        Log.d("GLOBAL", "GLOBAL_RESPONSE_: 200")
                        navController.navigate(R.id.movieFragment)
                        drawer.closeDrawer(GravityCompat.START)
                        return@OnNavigationItemSelectedListener true
                    }
                }
                true
            })
    }

}
