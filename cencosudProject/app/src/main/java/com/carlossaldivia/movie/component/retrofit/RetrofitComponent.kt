package com.carlossaldivia.movie.component.retrofit

//import com.carlossaldivia.movie.module.retrofit.RetrofitModule
import com.carlossaldivia.movie.module.retrofit.RetrofitModule
import com.carlossaldivia.movie.presentation.ui.fragments.HomeFragment
import dagger.Subcomponent
import javax.inject.Singleton

@Singleton
@Subcomponent(modules = arrayOf(RetrofitModule::class))
interface RetrofitComponent {
    // Classes that can be injected by this Component
    fun inject(homeFragment: HomeFragment)
//    fun inject(activity: RegistrationActivity)
//    fun inject(fragment: TermsAndConditionsFragment)

}