package com.carlossaldivia.movie.presentation.ui.fragments

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.carlossaldivia.movie.BuildConfig
import com.carlossaldivia.movie.R
import com.carlossaldivia.movie.data.interfaces.OnLoadMoreListener
import com.carlossaldivia.movie.data.interfaces.RecyclerListenerCallBacks
import com.carlossaldivia.movie.data.remote.ApiServiceFactory
import com.carlossaldivia.movie.domain.models.general.OnLoadMoreModel
import com.carlossaldivia.movie.domain.models.home.ResultMovie
import com.carlossaldivia.movie.presentation.adapter.MoviesAdapter
import com.carlossaldivia.movie.presentation.viewmodel.home.HomeViewModel
import com.carlossaldivia.movie.utils.Utils
import com.carlossaldivia.movie.utils.applyUISchedulers
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment(), RecyclerListenerCallBacks, OnLoadMoreListener {
    lateinit var viewModel: HomeViewModel

    private lateinit var moviesAdapter: MoviesAdapter;
    private lateinit var rvMovies: RecyclerView;

    private var totalPage = 0
    private var currentPage = 1
    private var firstTime = true
    var startIndex = 0


    private var validateScroll = false
    var mPreviousTotal = 0
    var mLoading = true
    var visibleThreshold = 8

    lateinit var navController: NavController

    companion object {
        fun newInstance() = HomeFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvMovies = view.findViewById(R.id.rvMovies)
        rvMovies.setHasFixedSize(true)
        rvMovies.setLayoutManager(GridLayoutManager(context, 2))

        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        progressBar.visibility = View.VISIBLE

        viewModel.retrofitApi = ApiServiceFactory.getInterface(activity as Activity, BuildConfig.BASE_URL)
        navController = Navigation.findNavController(activity as Activity, R.id.nav_host_fragment)
        getMoviesList("popularity.desc", currentPage)
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                      GET MOVIES LIST                                           //
////////////////////////////////////////////////////////////////////////////////////////////////////
    private fun getMoviesList(sort_by: String, page: Int) {
        viewModel
            .homeResponseApi(context, BuildConfig.API_KEY, sort_by, page)
            .applyUISchedulers()
            .subscribe (
                {
                    progressBar.visibility = View.GONE

                    viewModel.moviesList.addAll(it.results)

                    if (firstTime) {
                        firstTime = false
                        totalPage = it.total_pages
                        moviesAdapter = MoviesAdapter(
                            requireContext(),
                            viewModel.moviesList,
                            this)
                        Utils.setLazyLoadScrolling(rvMovies, validateScroll, mPreviousTotal, mLoading, visibleThreshold, currentPage, totalPage, this)
                    } else {
                        startIndex = viewModel.moviesList.size
                        moviesAdapter.resultMovies.addAll(it.results)
                    }

                    rvMovies.adapter = moviesAdapter
                    moviesAdapter.notifyDataSetChanged()

                    // Posicionando la vista del recycler a la mitad de la carga.
                    if (validateScroll == true) {
                        validateScroll = false
                        rvMovies.scrollToPosition(startIndex)
                    }

                    Utils.logGlobalGson("HOME_FRAGMENT_getMoviesList", it)
                    Utils.logGlobalString("HOME_FRAGMENT_getMoviesList_startIndex", startIndex.toString())
                    Utils.logGlobalString("HOME_FRAGMENT_getMoviesList_viewModel.moviesList.size", viewModel.moviesList.size.toString())
                },
                {
                    progressBar.visibility = View.GONE
                    Utils.logGlobalGson("HOME_FRAGMENT_ERROR", it)
                    Utils.showAlertDialog(
                        activity as Activity,
                        "error",
                        "",
                        it.message,
                        getString(R.string.action_accept),
                        "",
                        "HomeFragment_errorServer",
                        null
                    )
                })
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
//                                      INTERFACE RECYCLERVIE                                     //
////////////////////////////////////////////////////////////////////////////////////////////////////
    override fun onSuccess(message: String, objectSelected: String) {
        var gson = Gson()
        val type = object : TypeToken<ResultMovie>() {}.type
        var resultMovie: ResultMovie = gson.fromJson(objectSelected, type)
        Utils.logGlobalGson("HOME_FRAGMENT_onSuccess_resultMovie", resultMovie)
        val bundle = bundleOf("resultMovieArg" to resultMovie)
        navController.navigate(R.id.movieDetailFragment, bundle)
    }

    override fun onError(message: String, objectSelected: JsonObject) {
        TODO("Not yet implemented")
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
//                                      INTERFACE ONLOADMORE                                      //
////////////////////////////////////////////////////////////////////////////////////////////////////
    override fun onLoadMore(message: String, objectSelected: String) {
        if (message == "onLoadMoreOk") {
            var gson = Gson()
            val type = object : TypeToken<OnLoadMoreModel>() {}.type
            var onLoadMoreModel: OnLoadMoreModel = gson.fromJson(objectSelected, type)

            validateScroll = onLoadMoreModel.validateScroll
            mPreviousTotal = onLoadMoreModel.mPreviousTotal
            mLoading = onLoadMoreModel.mLoading
            currentPage = onLoadMoreModel.currentPage
            visibleThreshold = onLoadMoreModel.visibleThreshold

            Utils.logGlobalGson("HOME_FRAGMENT_onLoadMoreModel", onLoadMoreModel)
            getMoviesList("popularity.desc", currentPage)
        } else if (message == "onLoadMoreFinish") {
            Utils.showAlertDialog(
                activity as Activity,
                "error",
                "",
                getString(R.string.no_more_movies),
                getString(R.string.action_accept),
                "",
                "HomeFragment_onLoadMoreFinish",
                null
            )
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.moviesList = mutableListOf<ResultMovie>()
        totalPage = 0
        currentPage = 1
        firstTime = true
        startIndex = 0
        validateScroll = false
        mPreviousTotal = 0
        mLoading = true
        visibleThreshold = 8
        rvMovies.clearOnScrollListeners()
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
}