package com.carlossaldivia.movie.presentation.viewmodel

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel
import com.carlossaldivia.movie.BuildConfig
import com.carlossaldivia.movie.data.remote.ApiServiceFactory
import com.carlossaldivia.movie.domain.models.movie.CastDetailModel
import io.reactivex.Flowable
import java.util.concurrent.TimeUnit

class CastDetailViewModel : ViewModel() {
    var isLoading: ObservableBoolean = ObservableBoolean(false)

    fun castDetailResponseApi(context: Context?, personId: Long, api_key: String): Flowable<CastDetailModel> {
        var retrofitApi = ApiServiceFactory.getInterface(context!!, BuildConfig.BASE_URL)
        isLoading.set(true)
        return retrofitApi
            .getCastDetailResponse(personId, api_key)
            .throttleFirst(1, TimeUnit.SECONDS)
            .doOnError { isLoading.set(false) }
            .doOnComplete { isLoading.set(false) }
    }
}