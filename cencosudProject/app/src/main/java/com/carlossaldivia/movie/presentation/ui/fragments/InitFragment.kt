package com.carlossaldivia.movie.presentation.ui.fragments

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.carlossaldivia.movie.R
import com.carlossaldivia.movie.presentation.viewmodel.InitViewModel

class InitFragment : Fragment(){
    private lateinit var drawer: DrawerLayout
    private lateinit var toggle: ActionBarDrawerToggle
    private lateinit var viewFragment: View


    companion object {
        fun newInstance() = InitFragment()
    }

    private lateinit var viewModel: InitViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewFragment = inflater.inflate(R.layout.fragment_init, container, false)
        return viewFragment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUI(view)
    }

    fun initUI(view: View) {
    }

}