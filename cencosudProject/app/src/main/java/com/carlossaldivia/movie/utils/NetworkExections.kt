package com.carlossaldivia.movie.utils

import java.io.IOException

class NetworkExceptions {
    fun NoInternetException(message: String?): IOException {
        return IOException(message)
    }
}