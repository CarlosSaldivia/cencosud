package com.carlossaldivia.movie.base

import android.app.Application
import com.carlossaldivia.movie.component.AppComponent
import com.carlossaldivia.movie.component.DaggerAppComponent
import com.carlossaldivia.movie.module.AppModule

class App : Application() {
    val component: AppComponent by lazy {
        DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
    }
}