package com.carlossaldivia.movie.data.interfaces

import com.google.gson.JsonObject

interface RecyclerListenerCallBacks {
    fun onSuccess(message:String, objectSelected: String)
    fun onError(message: String, objectSelected: JsonObject)
}