package com.carlossaldivia.movie.module
import com.carlossaldivia.movie.base.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
public class AppModule(val app: App) {
    @Provides
    @Singleton
    fun provideApp() = app
}