package com.carlossaldivia.movie.data.interfaces

interface ListenerCallBacks {
    fun onSuccess(message:String, objectSelected: String)
    fun onError(message: String, objectSelected: String)
}