package com.carlossaldivia.cast.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.carlossaldivia.movie.BuildConfig
import com.carlossaldivia.movie.R
import com.carlossaldivia.movie.data.interfaces.RecyclerListenerCallBacks
import com.carlossaldivia.movie.domain.models.movie.Cast
import com.google.gson.Gson

class CastAdapter(context: Context, result: MutableList<Cast>, private val recyclerListener: RecyclerListenerCallBacks?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val context: Context = context
    var resultCast: MutableList<Cast> = result
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CastAdapterViewHolder(
            LayoutInflater.from(context).inflate(R.layout.layout_cast, parent, false)
        )
    }

    private inner class CastAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var rCast: RelativeLayout = itemView.findViewById(R.id.rCast)
        var iMovie: ImageView = itemView.findViewById(R.id.iMovie)
        var tName: TextView = itemView.findViewById(R.id.tName)
        var tLastNAme: TextView = itemView.findViewById(R.id.tLastNAme)

        fun bind(position: Int) {
            val cast = resultCast[position]

            if (cast.profile_path != null) {
                val url = "${BuildConfig.BASE_URL_IMAGE}/${cast.profile_path}"
                Glide.with(context)
                    .load(url)
                    // .apply(RequestOptions().transform(CenterCrop(), RoundedCorners(50)))
                    .circleCrop()
                    .into(iMovie)
            } else {
                iMovie.setImageResource(R.drawable.ic_exclamation_triangle)
            }


            tName.setText(cast.original_name)
            tLastNAme.setText(cast.character)
            rCast.setOnClickListener {
                val gson = Gson()
                val json = gson.toJson(cast)
                recyclerListener?.onSuccess("CastItem_click", json)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CastAdapter.CastAdapterViewHolder).bind(position)
    }

    override fun getItemCount(): Int {
        return resultCast.size
    }

}