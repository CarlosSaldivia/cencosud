package com.carlossaldivia.movie.domain.models.movie

import com.google.gson.annotations.SerializedName

data class CastDetailModel (
    @SerializedName("adult")
    val adult: Boolean,
    @SerializedName("also_known_as")
    val also_known_as: MutableList<String>,
    @SerializedName("biography")
    val biography: String,
    @SerializedName("birthday")
    val birthday: String? = null,
    @SerializedName("deathday")
    val deathday: String? = null,
    @SerializedName("gender")
    val gender: Long,
    @SerializedName("homepage")
    val homepage: String? = null,
    @SerializedName("id")
    val id: Long,
    @SerializedName("imdb_id")
    val imdb_id: String,
    @SerializedName("known_for_department")
    val known_for_department: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("place_of_birth")
    val place_of_birth: String? = null,
    @SerializedName("popularity")
    val popularity: Double,
    @SerializedName("profile_path")
    val profile_path: String
)

