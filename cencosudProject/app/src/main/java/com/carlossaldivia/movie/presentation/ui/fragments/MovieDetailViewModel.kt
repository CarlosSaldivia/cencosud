package com.carlossaldivia.movie.presentation.ui.fragments

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel
import com.carlossaldivia.movie.BuildConfig
import com.carlossaldivia.movie.data.remote.ApiServiceFactory
import com.carlossaldivia.movie.domain.models.home.ResultMovie
import com.carlossaldivia.movie.domain.models.movie.MovieDetailModel
import io.reactivex.Flowable
import java.util.concurrent.TimeUnit

class MovieDetailViewModel : ViewModel() {
    var isLoading: ObservableBoolean = ObservableBoolean(false)

    fun movieDetailResponseApi(context: Context?, movieId: Long, api_key: String, append_to_response: String): Flowable<MovieDetailModel> {
        var retrofitApi = ApiServiceFactory.getInterface(context!!, BuildConfig.BASE_URL)
        isLoading.set(true)
        return retrofitApi
            .getMovieDetailResponse(movieId, api_key, append_to_response)
            .throttleFirst(1, TimeUnit.SECONDS)
            .doOnError { isLoading.set(false) }
            .doOnComplete { isLoading.set(false) }
    }
}