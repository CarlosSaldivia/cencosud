package com.carlossaldivia.movie.utils

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.ColorFilter
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.net.ConnectivityManager
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.app.ActivityCompat.shouldShowRequestPermissionRationale
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.carlossaldivia.movie.R
import com.carlossaldivia.movie.data.interfaces.ListenerCallBacks
import com.carlossaldivia.movie.data.interfaces.OnLoadMoreListener
import com.carlossaldivia.movie.domain.models.general.OnLoadMoreModel
import com.google.gson.Gson

class Utils {

    companion object {
        lateinit var contextUtils: Context
        lateinit var progressDialog: ProgressDialog
////////////////////////////////////////////////////////////////////////////////////////////////////
//                                        LOG GLOBAL PROGRAM                                      //
////////////////////////////////////////////////////////////////////////////////////////////////////
        fun logGlobalString(title: String, message: String?) {
            Log.d("GLOBAL", "GLOBAL_RESPONSE_$title: $message")
        }

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                  LOG GLOBAL FOR HTTP ANSWER                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////
        fun logGlobalGson(title: String, objeto: Any?) {
            Log.d("GLOBAL", "GLOBAL_RESPONSE_$title: " + Gson().toJson(objeto))
        }

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                  FUNCTION FOR VALIDATE INTERNET                                //
////////////////////////////////////////////////////////////////////////////////////////////////////
        fun validateInternet(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                          ALERT DIALOG                                          //
////////////////////////////////////////////////////////////////////////////////////////////////////
        fun showAlertDialog(
    activity: Activity,
    icon: String,
    title: String?,
    message: String?,
    button1: String?,
    button2: String?,
    where: String?,
    listener: ListenerCallBacks?
        ) {
            val dialogBuilder = AlertDialog.Builder(activity, R.style.CustomDialog)
            dialogBuilder.setCancelable(false)
            dialogBuilder.setPositiveButton("Done", null)
            dialogBuilder.setNegativeButton("Close", null)
            val alertDialog = dialogBuilder.create()
            alertDialog.show()
            val inflater = activity.layoutInflater
            val dialogView: View = inflater.inflate(R.layout.layout_alert_dialog, null)
            alertDialog.window!!.setContentView(dialogView)
            val iconImage = dialogView.findViewById<ImageView>(R.id.iconImage)
            var btnDialogPositive: Button = dialogView.findViewById<Button>(R.id.btnDialogPositive)
            btnDialogPositive.setText(button1)
            btnDialogPositive.background = activity.resources.getDrawable(R.drawable.background_solid_rouded_24)
            var btnDialogNegative: Button = dialogView.findViewById<Button>(R.id.btnDialogNegative)
            btnDialogNegative.setText(button2)
            var tTitle = dialogView.findViewById<TextView>(R.id.tTitle)
            tTitle.visibility = View.GONE
            if ( title!!.length > 0 ) {
                tTitle.visibility = View.VISIBLE
                tTitle.setText(title)
            }
            var tMessage = dialogView.findViewById<TextView>(R.id.tMessage)
            if ( message!!.length > 0 ) {
                tMessage.setText(message)
                tMessage.visibility = View.VISIBLE
            } else {
                tMessage.visibility = View.GONE
            }

            val lIconImage = dialogView.findViewById<LinearLayout>(R.id.lIconImage)
            lateinit var filter: ColorFilter

            if (icon === "ok") {
                iconImage.background = activity.resources.getDrawable(R.drawable.ic_check_circle)
            } else if (icon == "error") {
                iconImage.background = activity.resources.getDrawable(R.drawable.ic_error_alert)
                lIconImage.setBackgroundColor(activity.resources.getColor(R.color.colorLipstick))
                filter = PorterDuffColorFilter(
                    activity.resources.getColor(R.color.colorLipstick),
                    PorterDuff.Mode.SRC_IN
                )
            } else if (icon == "info") {
                iconImage.background = activity.resources.getDrawable(R.drawable.ic_warning_alert)
                lIconImage.setBackgroundColor(activity.resources.getColor(R.color.colorGoldenYellow))
                filter = PorterDuffColorFilter(
                    activity.resources.getColor(R.color.colorGoldenYellow),
                    PorterDuff.Mode.SRC_IN
                )
            } else if ( icon.length == 0 ) {
                lIconImage.visibility = View.GONE
                filter = PorterDuffColorFilter(
                    activity.resources.getColor(R.color.colorTransparent),
                    PorterDuff.Mode.SRC_IN
                )
            }

            if ( filter != null) {
                lIconImage.background = activity.resources.getDrawable(R.drawable.background_solid_rouded_transparent_radius_left_right)
                lIconImage.background.colorFilter = filter
            }

            if (TextUtils.isEmpty(button2)) {
                btnDialogNegative.setVisibility(View.GONE)
            } else {
                btnDialogNegative.setVisibility(View.VISIBLE)
            }

            btnDialogPositive.setOnClickListener {
                alertDialog.dismiss()
                if (where == "HomeFragment_error_internet") {
                    listener?.onError("ERROR_INTERNET", "")
                }
            }

            btnDialogNegative.setOnClickListener {
                alertDialog.dismiss()
            }

        }


////////////////////////////////////////////////////////////////////////////////////////////////////
//                                          PROGRESS DIALOG                                       //
////////////////////////////////////////////////////////////////////////////////////////////////////
        fun showProgressDialog(context: Context, message: String){
            progressDialog = ProgressDialog(context)
            progressDialog.window?.setBackgroundDrawable(context.resources.getDrawable(R.drawable.background_solid_rouded_white_24))
            progressDialog.setMessage(message)
            progressDialog.setCancelable(false)
            progressDialog.show()
        }

        fun hideProgressDialog() {
            if ( progressDialog != null ) {
                if ( progressDialog.isShowing ) {
                    progressDialog.dismiss()
                }
            }
        }

////////////////////////////////////////////////////////////////////////////////////////////////////
//                              VERIFICANDO PERMISOS                                              //
////////////////////////////////////////////////////////////////////////////////////////////////////
        fun validatePermission(context: Context, activity: Activity) {
            if (checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(context, Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(context, Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
            ) {
                // Toast.makeText(context, "Permiso concedido anteriormente", Toast.LENGTH_SHORT).show();
            }
            if (shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                shouldShowRequestPermissionRationale(activity, Manifest.permission.INTERNET) ||
                shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_NETWORK_STATE) ||
                shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)
            ) {
                showAlertDialog(activity, "ok", "Permnisos desactivados", "Es necesario obtener los persmiso solicitados.", "Aceptar", "", "validatePermission", null)
            } else {
                requestPermissions(activity,
                    arrayOf(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ),
                    100
                )
            }
        }

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                      PAGINATION                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////
        public fun setLazyLoadScrolling(recycler: RecyclerView, validateScroll: Boolean, mPreviousTotal: Int, mLoading: Boolean, visibleThreshold: Int, currentPage: Int, totalPage: Int, listener: OnLoadMoreListener) {
            var validateScroll: Boolean = validateScroll
            var mPreviousTotal: Int = mPreviousTotal
            var mLoading: Boolean = mLoading
            var currentPage: Int = currentPage
            val visibleThreshold: Int = visibleThreshold
            val totalPage: Int = totalPage
            var onLoadMoreModel =
                OnLoadMoreModel(
                    false,
                    0,
                    false,
                    0,
                    0
                )

            recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val totalItemCount = recyclerView.layoutManager?.itemCount
                    if (mLoading) {
                        if (totalItemCount != null) {
                            if (totalItemCount > mPreviousTotal) {
                                mLoading = false
                                mPreviousTotal = totalItemCount
                            }
                        }
                    }

                    if (totalItemCount != null) {
                        val itemPosition = (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                        if (!mLoading && (itemPosition + visibleThreshold) >= totalItemCount) {
                            currentPage = currentPage + 1
                            mLoading = true
                            validateScroll = true
                            onLoadMore()
                        }
                    }
                }

                @SuppressLint("CheckResult")
                fun onLoadMore() {
                    if (currentPage <= totalPage) {
                        onLoadMoreModel.validateScroll = validateScroll
                        onLoadMoreModel.mPreviousTotal = mPreviousTotal
                        onLoadMoreModel.mLoading = mLoading
                        onLoadMoreModel.currentPage = currentPage
                        onLoadMoreModel.visibleThreshold = visibleThreshold

                        val gson = Gson()
                        val json = gson.toJson(onLoadMoreModel)
                        listener.onLoadMore("onLoadMoreOk", json)
                    } else {
                        listener.onLoadMore("onLoadMoreFinish", "")
                    }
                }
            })
        }

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                      CHANGE FRAGMENT                                           //
////////////////////////////////////////////////////////////////////////////////////////////////////
        fun changeScreen(activity: Activity, previousPage: String?) {
            var navController = Navigation.findNavController(activity, R.id.nav_host_fragment)

            if (previousPage == null) {
                navController.navigate(R.id.homeFragment)
            } else if  (previousPage == "popularFragment" ){
                navController.navigate(R.id.popularFragment)
            } else if  (previousPage == "playingFragment" ){
                navController.navigate(R.id.playingFragment)
            } else if  (previousPage == "upcomingFragment" ){
                navController.navigate(R.id.upcomingFragment)
            } else if  (previousPage == "ratingFragment" ){
                navController.navigate(R.id.ratingFragment)
            }
        }
////////////////////////////////////////////////////////////////////////////////////////////////////

    }
}