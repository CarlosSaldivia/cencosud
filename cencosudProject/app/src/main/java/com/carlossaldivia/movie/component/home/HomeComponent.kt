package com.carlossaldivia.movie.component.home

import com.carlossaldivia.movie.module.home.HomeModule
import com.carlossaldivia.movie.presentation.ui.fragments.HomeFragment
import dagger.Subcomponent
import javax.inject.Singleton

@Singleton
@Subcomponent(modules = arrayOf(HomeModule::class))
interface HomeComponent {
    fun inject(fragment: HomeFragment)
}