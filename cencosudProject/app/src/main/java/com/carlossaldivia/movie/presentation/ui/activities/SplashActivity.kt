package com.carlossaldivia.movie.presentation.ui.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.carlossaldivia.movie.R

class SplashActivity : AppCompatActivity() {
    var mainIntent: Intent? = null
    var tiempoSplash = 4600

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        mainIntent = Intent(this@SplashActivity, MainActivity::class.java)
        Handler().postDelayed({
            this@SplashActivity.startActivity(mainIntent)
            finish()
        }, tiempoSplash.toLong()) //4200

    }
}