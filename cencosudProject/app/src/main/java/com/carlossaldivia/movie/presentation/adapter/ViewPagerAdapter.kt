package com.carlossaldivia.movie.presentation.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.carlossaldivia.movie.presentation.ui.fragments.PlayingFragment
import com.carlossaldivia.movie.presentation.ui.fragments.PopularFragment
import com.carlossaldivia.movie.presentation.ui.fragments.RatingFragment
import com.carlossaldivia.movie.presentation.ui.fragments.UpcomingFragment

internal class ViewPagerAdapter(
    var context: Context,
    fm: FragmentManager?,
    var totalTabs: Int
) :
    FragmentPagerAdapter(fm!!) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                PopularFragment()
            }
            1 -> {
                RatingFragment()
            }
            2 -> {
                UpcomingFragment()
            }
            3 -> {
                PlayingFragment()
            }
            else -> getItem(position)
        }
    }
    override fun getCount(): Int {
        return totalTabs
    }
}