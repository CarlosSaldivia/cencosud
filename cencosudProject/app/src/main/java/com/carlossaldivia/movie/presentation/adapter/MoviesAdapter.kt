package com.carlossaldivia.movie.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.carlossaldivia.movie.BuildConfig
import com.carlossaldivia.movie.R
import com.carlossaldivia.movie.data.interfaces.RecyclerListenerCallBacks
import com.carlossaldivia.movie.domain.models.home.ResultMovie
import com.google.gson.Gson

class MoviesAdapter(context: Context, result: MutableList<ResultMovie>, private val recyclerListener: RecyclerListenerCallBacks?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val context: Context = context
    var resultMovies: MutableList<ResultMovie> = result
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MoviesAdapterViewHolder(
            LayoutInflater.from(context).inflate(R.layout.layout_movies, parent, false)
        )
    }

    private inner class MoviesAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var rMovie: RelativeLayout = itemView.findViewById(R.id.rMovie)
        var iMovie: ImageView = itemView.findViewById(R.id.iMovie)
        var tTitle: TextView = itemView.findViewById(R.id.tTitle)

        fun bind(position: Int) {
            val movie = resultMovies[position]

            if (movie.backdrop_path != null) {
                val url = "${BuildConfig.BASE_URL_IMAGE}/${movie.backdrop_path}"
                Glide.with(context)
                    .load(url)
                    .apply(RequestOptions().transform(CenterCrop(), RoundedCorners(50)))
                    .into(iMovie)
            } else {
                iMovie.setImageResource(R.drawable.ic_exclamation_triangle)
            }


            tTitle.setText(movie.original_title)
            rMovie.setOnClickListener {
                val gson = Gson()
                val json = gson.toJson(movie)
                recyclerListener?.onSuccess("MoviesItem_click", json)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MoviesAdapter.MoviesAdapterViewHolder).bind(position)
    }

    override fun getItemCount(): Int {
        return resultMovies.size
    }

}