package com.carlossaldivia.movie.presentation.viewmodel.home

import android.app.Application
import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.carlossaldivia.movie.BuildConfig
import com.carlossaldivia.movie.data.api.RetrofitApi
import com.carlossaldivia.movie.data.remote.ApiServiceFactory
import com.carlossaldivia.movie.domain.models.home.HomeResponseModel
import com.carlossaldivia.movie.domain.models.home.ResultMovie
import io.reactivex.Flowable
import java.util.concurrent.TimeUnit

open class HomeViewModel() : ViewModel() {
    var isLoading: ObservableBoolean = ObservableBoolean(false)
    var moviesList = mutableListOf<ResultMovie>()
    lateinit var retrofitApi: RetrofitApi

    fun homeResponseApi(context: Context?, api_key: String, sort_by: String, page: Int): Flowable<HomeResponseModel> {
        isLoading.set(true)
        return retrofitApi
            .getHomeResponse(api_key, sort_by, page)
            .throttleFirst(1, TimeUnit.SECONDS)
            .doOnError { isLoading.set(false) }
            .doOnComplete { isLoading.set(false) }
    }

}