package com.carlossaldivia.movie.presentation.viewmodel

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel
import com.carlossaldivia.movie.BuildConfig
import com.carlossaldivia.movie.data.remote.ApiServiceFactory
import com.carlossaldivia.movie.domain.models.home.HomeResponseModel
import com.carlossaldivia.movie.domain.models.home.ResultMovie
import io.reactivex.Flowable
import java.util.concurrent.TimeUnit

class PopularViewModel : ViewModel() {
    var isLoading: ObservableBoolean = ObservableBoolean(false)
    var moviesList = mutableListOf<ResultMovie>()

    fun populateResponseApi(context: Context?, api_key: String, sort_by: String, page: Int): Flowable<HomeResponseModel> {
        var retrofitApi = ApiServiceFactory.getInterface(context!!, BuildConfig.BASE_URL)
        isLoading.set(true)
        return retrofitApi
            .getPopulateResponse(api_key, sort_by, page)
            .throttleFirst(1, TimeUnit.SECONDS)
            .doOnError { isLoading.set(false) }
            .doOnComplete { isLoading.set(false) }
    }
}