package com.carlossaldivia.movie.presentation.ui.fragments

import android.app.Activity
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.carlossaldivia.movie.BuildConfig
import com.carlossaldivia.movie.R
import com.carlossaldivia.movie.data.interfaces.OnLoadMoreListener
import com.carlossaldivia.movie.data.interfaces.RecyclerListenerCallBacks
import com.carlossaldivia.movie.utils.applyUISchedulers
import com.carlossaldivia.movie.domain.models.general.OnLoadMoreModel
import com.carlossaldivia.movie.domain.models.home.ResultMovie
import com.carlossaldivia.movie.presentation.adapter.MoviesAdapter
import com.carlossaldivia.movie.presentation.viewmodel.PopularViewModel
import com.carlossaldivia.movie.utils.Utils
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_home.*

class PopularFragment : Fragment(), RecyclerListenerCallBacks, OnLoadMoreListener {
    lateinit var viewModel: PopularViewModel

    private lateinit var moviesAdapter: MoviesAdapter;
    private lateinit var rvMovies: RecyclerView;

    private var totalPage = 0
    private var currentPage = 1
    private var firstTime = true
    var startIndex = 0


    private var validateScroll = false
    var mPreviousTotal = 0
    var mLoading = true
    var visibleThreshold = 8

    companion object {
        fun newInstance() = PopularFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_popular, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvMovies = view.findViewById(R.id.rvMovies)
        rvMovies.setHasFixedSize(true)
        rvMovies.setLayoutManager(GridLayoutManager(context, 2))

        viewModel = ViewModelProviders.of(this).get(PopularViewModel::class.java)
        progressBar.visibility = View.VISIBLE

        getMoviesList("popularity.asc", currentPage)

    }

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                      GET MOVIES LIST                                           //
////////////////////////////////////////////////////////////////////////////////////////////////////
    private fun getMoviesList(sort_by: String, page: Int) {
        viewModel
            .populateResponseApi(context, BuildConfig.API_KEY, sort_by, page)
            .applyUISchedulers()
            .subscribe (
                {
                    progressBar.visibility = View.GONE

                    viewModel.moviesList.addAll(it.results)

                    if (firstTime) {
                        firstTime = false
                        totalPage = it.total_pages
                        moviesAdapter = MoviesAdapter(
                            requireContext(),
                            viewModel.moviesList,
                            this)
                        Utils.setLazyLoadScrolling(rvMovies, validateScroll, mPreviousTotal, mLoading, visibleThreshold, currentPage, totalPage, this)
                    } else {
                        startIndex = viewModel.moviesList.size
                        moviesAdapter.resultMovies.addAll(it.results)
                    }

                    rvMovies.adapter = moviesAdapter
                    moviesAdapter.notifyDataSetChanged()

                    // Posicionando la vista del recycler a la mitad de la carga.
                    if (validateScroll == true) {
                        validateScroll = false
                        rvMovies.scrollToPosition(startIndex)
                    }

                    Utils.logGlobalGson("POPULAR_FRAGMENT_getMoviesList", it)
                    Utils.logGlobalString("POPULAR_FRAGMENT_getMoviesList_startIndex", startIndex.toString())
                    Utils.logGlobalString("POPULAR_FRAGMENT_getMoviesList_viewModel.moviesList.size", viewModel.moviesList.size.toString())
                },
                {
                    progressBar.visibility = View.GONE
                    Utils.logGlobalGson("POPULAR_FRAGMENT_ERROR", it)
                    Utils.showAlertDialog(
                        activity as Activity,
                        "error",
                        "",
                        it.message,
                        getString(R.string.action_accept),
                        "",
                        "POPULAR_FRAGMENT_errorServer",
                        null
                    )
                })
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
//                                      INTERFACE RECYCLERVIE                                     //
////////////////////////////////////////////////////////////////////////////////////////////////////
    override fun onSuccess(message: String, objectSelected: String) {
        var gson = Gson()
        val type = object : TypeToken<ResultMovie>() {}.type
        var resultMovie: ResultMovie = gson.fromJson(objectSelected, type)
        Utils.logGlobalGson("POPULAR_FRAGMENT_onSuccess_resultMovie", resultMovie)
        val bundle = bundleOf("resultMovieArg" to resultMovie, "previousPage" to "popularFragment")
        var navController = Navigation.findNavController(activity as Activity, R.id.nav_host_fragment)
        navController.navigate(R.id.movieDetailFragment, bundle)
        requireFragmentManager().beginTransaction().remove(this).commit()
    }

    override fun onError(message: String, objectSelected: JsonObject) {
        TODO("Not yet implemented")
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
//                                      INTERFACE ONLOADMORE                                      //
////////////////////////////////////////////////////////////////////////////////////////////////////
    override fun onLoadMore(message: String, objectSelected: String) {
        if (message == "onLoadMoreOk") {
            var gson = Gson()
            val type = object : TypeToken<OnLoadMoreModel>() {}.type
            var onLoadMoreModel: OnLoadMoreModel = gson.fromJson(objectSelected, type)

            validateScroll = onLoadMoreModel.validateScroll
            mPreviousTotal = onLoadMoreModel.mPreviousTotal
            mLoading = onLoadMoreModel.mLoading
            currentPage = onLoadMoreModel.currentPage
            visibleThreshold = onLoadMoreModel.visibleThreshold

            Utils.logGlobalGson("POPULAR_FRAGMENT_onLoadMoreModel", onLoadMoreModel)
            getMoviesList("popularity.asc", currentPage)
        } else if (message == "onLoadMoreFinish") {
            Utils.showAlertDialog(
                activity as Activity,
                "error",
                "",
                getString(R.string.no_more_movies),
                getString(R.string.action_accept),
                "",
                "POPULAR_FRAGMENT_onLoadMoreFinish",
                null
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.moviesList = mutableListOf<ResultMovie>()
        totalPage = 0
        currentPage = 1
        firstTime = true
        startIndex = 0
        validateScroll = false
        mPreviousTotal = 0
        mLoading = true
        visibleThreshold = 8
        rvMovies.clearOnScrollListeners()
    }
////////////////////////////////////////////////////////////////////////////////////////////////////



}