package com.carlossaldivia.movie.utils

import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


// RX
fun <T> Flowable<T>.applyUISchedulers()
        = this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

fun <T> Flowable<T>.applyTestSchedulers()
        = this.subscribeOn(Schedulers.trampoline()).observeOn(Schedulers.trampoline())

fun <T> Observable<T>.applyUISchedulers()
        = this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

fun <T> Observable<T>.applyTestSchedulers()
        = this.subscribeOn(Schedulers.trampoline()).observeOn(Schedulers.trampoline())

