package com.carlossaldivia.movie.domain.models.home

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HomeResponseModel (
    @SerializedName("page")
    val page: Long,
    @SerializedName("results")
    val results: MutableList<ResultMovie>,
    @SerializedName("total_pages")
    val total_pages: Int,
    @SerializedName("total_results")
    val total_results: Int
) : Parcelable




