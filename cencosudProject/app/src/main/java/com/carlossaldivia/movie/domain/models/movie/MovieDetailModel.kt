package com.carlossaldivia.movie.domain.models.movie

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class MovieDetailModel (
    @SerializedName("adult")
    val adult: Boolean,
    @SerializedName("backdrop_path")
    val backdrop_path: String,
    // @SerializedName("belongs_to_collection")
    // val belongs_to_collection: Any? = null,
    @SerializedName("budget")
    val budget: Long,
    @SerializedName("genres")
    val genres: MutableList<Genre>,
    @SerializedName("homepage")
    val homepage: String,
    @SerializedName("id")
    val id: Long,
    @SerializedName("imdb_id")
    val imdb_id: String,
    @SerializedName("original_language")
    val original_language: String,
    @SerializedName("original_title")
    val original_title: String,
    @SerializedName("overview")
    val overview: String,
    @SerializedName("popularity")
    val popularity: Double,
    @SerializedName("poster_path")
    val poster_path: String,
    @SerializedName("production_companies")
    val production_companies: MutableList<ProductionCompany>,
    @SerializedName("production_countries")
    val production_countries: MutableList<ProductionCountry>,
    @SerializedName("release_date")
    val release_date: String,
    @SerializedName("revenue")
    val revenue: Long,
    @SerializedName("runtime")
    val runtime: Long,
    @SerializedName("spoken_languages")
    val spoken_languages: MutableList<SpokenLanguage>,
    @SerializedName("status")
    val status: String,
    @SerializedName("tagline")
    val tagline: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("video")
    val video: Boolean,
    @SerializedName("vote_average")
    val vote_average: Double,
    @SerializedName("voteCount")
    val voteCount: Long,
    @SerializedName("credits")
    val credits: Credits
): Parcelable

@Parcelize
data class Credits (
    @SerializedName("cast")
    val cast: MutableList<Cast>,
    @SerializedName("crew")
    val crew: MutableList<Cast>
): Parcelable

@Parcelize
data class Cast (
    @SerializedName("adult")
    val adult: Boolean,
    @SerializedName("gender")
    val gender: Long,
    @SerializedName("id")
    val id: Long,
    @SerializedName("known_for_department")
    val known_for_department: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("original_name")
    val original_name: String,
    @SerializedName("popularity")
    val popularity: Double,
    @SerializedName("profile_path")
    val profile_path: String? = null,
    @SerializedName("cast_id")
    val cast_id: Long? = null,
    @SerializedName("character")
    val character: String? = null,
    @SerializedName("credit_id")
    val credit_id: String,
    @SerializedName("order")
    val order: Long? = null,
    @SerializedName("department")
    val department: String? = null,
    @SerializedName("job")
    val job: String? = null
): Parcelable

@Parcelize
data class Genre (
    @SerializedName("id")
    val id: Long,
    @SerializedName("name")
    val name: String
): Parcelable

@Parcelize
data class ProductionCompany (
    @SerializedName("id")
    val id: Long,
    @SerializedName("logo_path")
    val logo_path: String? = null,
    @SerializedName("name")
    val name: String,
    @SerializedName("origin_country")
    val origin_country: String
): Parcelable

@Parcelize
data class ProductionCountry (
    @SerializedName("iso3166_1")
    val iso3166_1: String,
    @SerializedName("name")
    val name: String
): Parcelable

@Parcelize
data class SpokenLanguage (
    @SerializedName("english_name")
    val english_name: String,
    @SerializedName("iso639_1")
    val iso639_1: String,
    @SerializedName("name")
    val name: String
): Parcelable
