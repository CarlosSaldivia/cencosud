package com.carlossaldivia.movie.presentation.ui.fragments

import android.app.Activity
import android.os.Bundle
import android.text.Html
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.request.RequestOptions
import com.carlossaldivia.cast.presentation.adapter.CastAdapter
import com.carlossaldivia.images.presentation.adapter.ImagesAdapter
import com.carlossaldivia.movie.BuildConfig
import com.carlossaldivia.movie.R
import com.carlossaldivia.movie.data.interfaces.RecyclerListenerCallBacks
import com.carlossaldivia.movie.utils.applyUISchedulers
import com.carlossaldivia.movie.domain.models.home.ResultMovie
import com.carlossaldivia.movie.domain.models.movie.Cast
import com.carlossaldivia.movie.domain.models.movie.MovieDetailModel
import com.carlossaldivia.movie.utils.Utils
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_home.*

class MovieDetailFragment : Fragment(), RecyclerListenerCallBacks {
    private lateinit var viewModel: MovieDetailViewModel
    private lateinit var movieDetailModel: MovieDetailModel

    private lateinit var iPoster: ImageView
    private lateinit var tTitle: TextView
    private lateinit var tDate: TextView
    private lateinit var tGender: TextView
    private lateinit var tStatusMessage: TextView
    private lateinit var tRuntimeMessage: TextView
    private lateinit var tOverviewMessage: TextView
    private lateinit var tHomePageMessage: TextView
    private lateinit var rvCastList: RecyclerView
    private lateinit var castAdapter: CastAdapter
    private lateinit var rvImagesList: RecyclerView
    private lateinit var imagesAdapter: ImagesAdapter

    lateinit var navController: NavController

    var previousPage: String? = null

    companion object {
        fun newInstance() = MovieDetailFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movie_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUI(view)
    }

    fun initUI(view: View) {
        rvCastList = view.findViewById(R.id.rvCastList)
        rvCastList.setHasFixedSize(true)
        rvCastList.setNestedScrollingEnabled(false)
        rvCastList.layoutManager = LinearLayoutManager(activity as Activity, LinearLayoutManager.HORIZONTAL ,false)

        rvImagesList = view.findViewById(R.id.rvImagesList)
        rvImagesList.setHasFixedSize(true)
        rvImagesList.setNestedScrollingEnabled(false)
        rvImagesList.layoutManager = LinearLayoutManager(activity as Activity, LinearLayoutManager.HORIZONTAL ,false)


        viewModel = ViewModelProviders.of(this).get(MovieDetailViewModel::class.java)
        val resultArg: ResultMovie = arguments?.get("resultMovieArg") as ResultMovie
        previousPage = arguments?.getString("previousPage")
        progressBar.visibility = View.VISIBLE

        iPoster = view.findViewById(R.id.iPoster)
        if (resultArg.poster_path != null) {
            val url = "${BuildConfig.BASE_URL_IMAGE}/${resultArg.poster_path}"
            Glide.with(activity as Activity)
                .load(url)
                .override(600, 1200)
                .apply(RequestOptions().transform(CenterCrop()))
                // .circleCrop()
                // .fitCenter()
                .into(iPoster)
        } else {
            iPoster.setImageResource(R.drawable.ic_exclamation_triangle)
        }

        tTitle = view.findViewById(R.id.tTitle)
        tDate = view.findViewById(R.id.tDate)
        tGender = view.findViewById(R.id.tGender)
        tStatusMessage = view.findViewById(R.id.tStatusMessage)
        tRuntimeMessage = view.findViewById(R.id.tRuntimeMessage)
        tOverviewMessage = view.findViewById(R.id.tOverviewMessage)
        tHomePageMessage = view.findViewById(R.id.tHomePageMessage)


        navController = Navigation.findNavController(activity as Activity, R.id.nav_host_fragment)

        getMovieDetail(resultArg.id, "credits")
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                      GET MOVIES LIST                                           //
////////////////////////////////////////////////////////////////////////////////////////////////////
    private fun getMovieDetail(movieId: Long, append_to_response: String) {
        viewModel
            .movieDetailResponseApi(context, movieId, BuildConfig.API_KEY, append_to_response)
            .applyUISchedulers()
            .subscribe (
                {
                    progressBar.visibility = View.GONE

                    movieDetailModel = it
                    loadData(movieDetailModel)
                },
                {
                    progressBar.visibility = View.GONE
                    Utils.logGlobalGson("MOVIE_DETAIL_FRAGMENT_ERROR", it)
                    Utils.showAlertDialog(
                        activity as Activity,
                        "error",
                        "",
                        it.message,
                        getString(R.string.action_accept),
                        "",
                        "MOVIE_DETAIL_errorServer",
                        null
                    )
                })
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
//                                      LOAD DATA API                                             //
////////////////////////////////////////////////////////////////////////////////////////////////////
    fun loadData(movieDetailModel: MovieDetailModel){
        tTitle.setText(movieDetailModel.original_title)
        tDate.setText(movieDetailModel.release_date)
        var gender = ""
        var pos = 0
        for (genderItem in movieDetailModel.genres){
            if (pos < movieDetailModel.genres.size) {
                gender = gender + genderItem.name + ", "
            } else {
                gender = gender + genderItem.name + "."
            }
            pos++
        }
        tGender.setText(gender)
        tStatusMessage.setText(movieDetailModel.status)
        tRuntimeMessage.setText(movieDetailModel.runtime.toString())
        tOverviewMessage.setText(movieDetailModel.overview)
        tHomePageMessage.setText(Html.fromHtml(movieDetailModel.homepage))

        castAdapter = CastAdapter(
            requireContext(),
            movieDetailModel.credits.cast,
            this)
        rvCastList.adapter = castAdapter
        castAdapter.notifyDataSetChanged()

        val imagesList = mutableListOf<String>()
        imagesList.add(movieDetailModel.backdrop_path)
        imagesList.add(movieDetailModel.poster_path)
        imagesAdapter = ImagesAdapter(
            requireContext(),
            imagesList,
            this)
        rvImagesList.adapter = imagesAdapter
        imagesAdapter.notifyDataSetChanged()
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
//                                      INTERFACE RECYCLERVIE                                     //
////////////////////////////////////////////////////////////////////////////////////////////////////
    override fun onSuccess(message: String, objectSelected: String) {
        if (message == "CastItem_click") {
            var gson = Gson()
            val type = object : TypeToken<Cast>() {}.type
            var cast: Cast = gson.fromJson(objectSelected, type)
            Utils.logGlobalGson("MOVIE_DETAIL_FRAGMENT_onSuccess_cast", cast)
            // val bundle = bundleOf("castArg" to cast)
            val bundle = bundleOf("castArg" to cast, "previousPage" to previousPage)
            navController.navigate(R.id.castDetailFragment, bundle)
        }
    }

    override fun onError(message: String, objectSelected: JsonObject) {
        TODO("Not yet implemented")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        // Utils.changeScreen(activity as Activity, previousPage)
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
}