package com.carlossaldivia.movie.presentation.ui.fragments

import android.app.Activity
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.Html
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.request.RequestOptions
import com.carlossaldivia.cast.presentation.adapter.CastAdapter
import com.carlossaldivia.images.presentation.adapter.ImagesAdapter
import com.carlossaldivia.movie.BuildConfig
import com.carlossaldivia.movie.R
import com.carlossaldivia.movie.utils.applyUISchedulers
import com.carlossaldivia.movie.domain.models.home.ResultMovie
import com.carlossaldivia.movie.domain.models.movie.Cast
import com.carlossaldivia.movie.domain.models.movie.CastDetailModel
import com.carlossaldivia.movie.domain.models.movie.MovieDetailModel
import com.carlossaldivia.movie.presentation.viewmodel.CastDetailViewModel
import com.carlossaldivia.movie.utils.Utils
import kotlinx.android.synthetic.main.fragment_home.*

class CastDetailFragment : Fragment() {
    private lateinit var viewModel: CastDetailViewModel
    private lateinit var castDetailModel: CastDetailModel

    private lateinit var iActor: ImageView
    private lateinit var tNameMessage: TextView
    private lateinit var tBirthdayMessage: TextView
    private lateinit var tPlaceBirthdayMessage: TextView
    private lateinit var tHomePageMessage: TextView

    companion object {
        fun newInstance() = CastDetailFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_cast_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CastDetailViewModel::class.java)
        initUI(view)
    }

    fun initUI(view: View) {
        iActor = view.findViewById(R.id.iActor)
        tNameMessage = view.findViewById(R.id.tNameMessage)
        tBirthdayMessage = view.findViewById(R.id.tBirthdayMessage)
        tPlaceBirthdayMessage = view.findViewById(R.id.tPlaceBirthdayMessage)
        tHomePageMessage = view.findViewById(R.id.tHomePageMessage)

        val resultArg: Cast = arguments?.get("castArg") as Cast
        progressBar.visibility = View.VISIBLE
        getCastDetail(resultArg.id)
    }


////////////////////////////////////////////////////////////////////////////////////////////////////
//                                      GET MOVIES LIST                                           //
////////////////////////////////////////////////////////////////////////////////////////////////////
    private fun getCastDetail(personaId: Long) {
        viewModel
            .castDetailResponseApi(context, personaId, BuildConfig.API_KEY)
            .applyUISchedulers()
            .subscribe (
                {
                    progressBar.visibility = View.GONE

                    castDetailModel = it
                    loadData(castDetailModel)
                },
                {
                    progressBar.visibility = View.GONE
                    Utils.logGlobalGson("CAST_DETAIL_FRAGMENT_ERROR", it)
                    Utils.showAlertDialog(
                        activity as Activity,
                        "error",
                        "",
                        it.message,
                        getString(R.string.action_accept),
                        "",
                        "CAST_DETAIL_errorServer",
                        null
                    )
                })
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
//                                      LOAD DATA API                                             //
////////////////////////////////////////////////////////////////////////////////////////////////////
    fun loadData(castDetailModel: CastDetailModel){
        if (castDetailModel.profile_path != null) {
            val url = "${BuildConfig.BASE_URL_IMAGE}/${castDetailModel.profile_path}"
            Glide.with(activity as Activity)
                .load(url)
                .circleCrop()
                .into(iActor)
        } else {
            iActor.setImageResource(R.drawable.ic_exclamation_triangle)
        }

        var name = "-"
        if (castDetailModel.name != null) {
            name = castDetailModel.name
        }
        var birthday = "-"
        if (castDetailModel.birthday != null) {
            birthday = castDetailModel.birthday
        }
        var homepage = "-"
        if (castDetailModel.homepage != null) {
            homepage = castDetailModel.homepage
        }
        var place_of_birth = "-"
        if (castDetailModel.place_of_birth != null) {
            place_of_birth = castDetailModel.place_of_birth
        }

        tNameMessage.setText(name)
        tBirthdayMessage.setText(birthday)
        tPlaceBirthdayMessage.setText(place_of_birth)
        tHomePageMessage.setText(homepage)
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
}