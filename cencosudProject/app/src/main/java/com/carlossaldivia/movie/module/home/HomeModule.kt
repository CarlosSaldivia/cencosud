package com.carlossaldivia.movie.module.home

import com.carlossaldivia.movie.presentation.ui.fragments.HomeFragment
import dagger.Module

@Module
class HomeModule(val fragment: HomeFragment) {
}