package com.carlossaldivia.movie.data.remote

import android.app.Activity
import android.content.Context
import com.carlossaldivia.movie.R
import com.carlossaldivia.movie.utils.Utils
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException


class UnauthorizedInterceptor(private val context: Context) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)
        val responseCode = response.code
        if (responseCode == 401 || responseCode == 403) {
            val activity = context as Activity
            activity.runOnUiThread {
                Utils.showAlertDialog(
                    activity,
                    "info",
                    "",
                    "Sesión expirada",
                    context.getString(
                        R.string.action_accept
                    ),
                    "",
                    "UnauthorizedInterceptor_profile",
                    null
                )
            }


        }
        return response
    }
}