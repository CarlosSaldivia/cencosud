package com.carlossaldivia.movie.data.api

import com.carlossaldivia.movie.domain.models.home.HomeResponseModel
import com.carlossaldivia.movie.domain.models.movie.CastDetailModel
import com.carlossaldivia.movie.domain.models.movie.MovieDetailModel
import io.reactivex.Flowable
import retrofit2.http.*

interface RetrofitApi {
    @Headers(
        "Accept: */*",
        "Content-Type: application/json",
        "x-mock-match-request-body: true",
        "Connection: keep-alive"
    )
    @GET("3/discover/movie")
    fun getHomeResponse(@Query("api_key") api_key: String, @Query("sort_by") sort_by: String, @Query("page") page: Int): Flowable<HomeResponseModel>

    @GET("3/discover/movie")
    fun getPopulateResponse(@Query("api_key") api_key: String, @Query("sort_by") sort_by: String, @Query("page") page: Int): Flowable<HomeResponseModel>

    @GET("3/discover/movie")
    fun getRatingResponse(@Query("api_key") api_key: String, @Query("vote_average.gte") vote_average: Int, @Query("page") page: Int): Flowable<HomeResponseModel>

    @GET("3/discover/movie")
    fun getUpcomingResponse(@Query("api_key") api_key: String, @Query("primary_release_date.gte") primary_release_date: Int, @Query("page") page: Int): Flowable<HomeResponseModel>

    @GET("3/trending/movie/day")
    fun getPlayingResponse(@Query("api_key") api_key: String, @Query("page") page: Int): Flowable<HomeResponseModel>

    @GET("3/movie/{movieId}")
    fun getMovieDetailResponse(@Path("movieId") movieId:Long, @Query("api_key") api_key: String, @Query("append_to_response") append_to_response: String): Flowable<MovieDetailModel>

    @GET("3/person/{personId}")
    fun getCastDetailResponse(@Path("personId") personId:Long, @Query("api_key") api_key: String): Flowable<CastDetailModel>
}