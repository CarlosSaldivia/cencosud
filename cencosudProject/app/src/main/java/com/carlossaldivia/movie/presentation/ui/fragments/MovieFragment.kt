package com.carlossaldivia.movie.presentation.ui.fragments

import android.app.Activity
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager
import com.carlossaldivia.movie.R
import com.carlossaldivia.movie.presentation.adapter.ViewPagerAdapter
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_movie.*

class MovieFragment : Fragment(){

    val tabLayout: TabLayout get() = tablLayout.findViewById(R.id.tabLayout)
    val viewPager: ViewPager get() = tablLayout.findViewById(R.id.viewPager)

    private val tabIcons = intArrayOf(
        R.drawable.ic_desktop,
        R.drawable.ic_desktop,
        R.drawable.ic_desktop,
        R.drawable.ic_desktop
    )

    companion object {
        fun newInstance() = MovieFragment()
    }

    private lateinit var viewModel: MovieViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movie, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MovieViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        initUI(view)
    }

    fun initUI(view: View) {

        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_layout_popular).setIcon(tabIcons[0]))
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_layout_rating).setIcon(tabIcons[1]))
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_layout_upcoming).setIcon(tabIcons[2]))
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_layout_playing).setIcon(tabIcons[3]))
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = ViewPagerAdapter(activity as Activity, fragmentManager, tabLayout.tabCount)
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

}