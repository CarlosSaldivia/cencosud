package com.carlossaldivia.images.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.carlossaldivia.movie.BuildConfig
import com.carlossaldivia.movie.R
import com.carlossaldivia.movie.data.interfaces.RecyclerListenerCallBacks
import com.google.gson.Gson

class ImagesAdapter(context: Context, result: MutableList<String>, private val recyclerListener: RecyclerListenerCallBacks?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val context: Context = context
    var resultImages: MutableList<String> = result
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ImagesAdapterViewHolder(
            LayoutInflater.from(context).inflate(R.layout.layout_images, parent, false)
        )
    }

    private inner class ImagesAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var rImages: RelativeLayout = itemView.findViewById(R.id.rImages)
        var iPoster: ImageView = itemView.findViewById(R.id.iPoster)

        fun bind(position: Int) {
            val images = resultImages[position]

            if (images != null ){
                if (images.length > 0) {
                    val url = "${BuildConfig.BASE_URL_IMAGE}/${images}"
                    Glide.with(context)
                        .load(url)
                        .apply(RequestOptions().transform(CenterCrop(), RoundedCorners(50)))
                        // .circleCrop()
                        .into(iPoster)
                }
            } else {
                iPoster.setImageResource(R.drawable.ic_exclamation_triangle)
            }


            rImages.setOnClickListener {
                val gson = Gson()
                val json = gson.toJson(images)
                recyclerListener?.onSuccess("ImagesItem_click", json)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ImagesAdapter.ImagesAdapterViewHolder).bind(position)
    }

    override fun getItemCount(): Int {
        return resultImages.size
    }

}