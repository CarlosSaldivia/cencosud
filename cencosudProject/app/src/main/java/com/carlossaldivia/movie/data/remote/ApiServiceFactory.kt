package com.carlossaldivia.movie.data.remote

import android.content.Context
import com.carlossaldivia.movie.data.api.RetrofitApi
import com.google.gson.GsonBuilder
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


object ApiServiceFactory {
    private var retrofit: Retrofit? = null

    fun getInterface(context: Context, baseUrl: String): RetrofitApi {
        if (retrofit == null) {
            retrofit = apiServiceFactory(context, baseUrl)
        }
        return retrofit!!.create(RetrofitApi::class.java)
    }

    fun apiServiceFactory(context: Context, baseUrl: String): Retrofit {
        val httpClient = OkHttpClient.Builder()
        val networkConnectionInterceptor = NetworkConnectionInterceptor(context)
        val unauthorizedInterceptor = UnauthorizedInterceptor(context)
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging)
        httpClient.addInterceptor(networkConnectionInterceptor)
        httpClient.addInterceptor(unauthorizedInterceptor)
        httpClient.connectTimeout(30, TimeUnit.SECONDS).retryOnConnectionFailure(true)

        return Retrofit.Builder().baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(getUnsafeOkHttpClient(context))
            .build()
    }

    fun getUnsafeOkHttpClient(context: Context): OkHttpClient? {
        return try {
            // Create a trust manager that does not validate certificate chains
            val trustAllCerts = arrayOf<TrustManager>(
                object : X509TrustManager {
                    @Throws(CertificateException::class)
                    override fun checkClientTrusted(
                        chain: Array<X509Certificate>,
                        authType: String
                    ) {
                    }

                    @Throws(CertificateException::class)
                    override fun checkServerTrusted(
                        chain: Array<X509Certificate>,
                        authType: String
                    ) {
                    }

                    override fun getAcceptedIssuers(): Array<X509Certificate> {
                        return arrayOf()
                    }
                }
            )

            // Validate SSL certificate
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, SecureRandom())

            // Create an ssl socket factory with our all-trusting manager
            val sslSocketFactory = sslContext.socketFactory
            val logging = HttpLoggingInterceptor()
            logging.setLevel(HttpLoggingInterceptor.Level.BODY)
            val networkConnectionInterceptor = NetworkConnectionInterceptor(context)
            val unauthorizedInterceptor = UnauthorizedInterceptor(context)
            val builder = OkHttpClient.Builder()
            builder.addInterceptor(logging)
            builder.addInterceptor(networkConnectionInterceptor)
            builder.addInterceptor(unauthorizedInterceptor)
            builder.sslSocketFactory(
                sslSocketFactory,
                (trustAllCerts[0] as X509TrustManager)
            )
            // builder.hostnameVerifier { hostname, session -> true }
            builder.build()
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }
}