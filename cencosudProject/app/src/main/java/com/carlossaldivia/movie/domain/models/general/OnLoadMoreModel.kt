package com.carlossaldivia.movie.domain.models.general

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OnLoadMoreModel (
    @SerializedName("validateScroll")
    var validateScroll: Boolean,
    @SerializedName("mPreviousTotal")
    var mPreviousTotal: Int,
    @SerializedName("mLoading")
    var mLoading: Boolean,
    @SerializedName("currentPage")
    var currentPage: Int,
    @SerializedName("visibleThreshold")
    var visibleThreshold: Int
) : Parcelable