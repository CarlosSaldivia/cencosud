package com.carlossaldivia.movie.data.remote

import android.content.Context
import android.net.wifi.WifiManager
import com.carlossaldivia.movie.R
import com.carlossaldivia.movie.utils.NetworkExceptions
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import kotlin.jvm.Throws

class NetworkConnectionInterceptor(private val context: Context) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!checkWifi(context)) {
            throw NetworkExceptions().NoInternetException(context.resources.getString(R.string.error_connection_wifi))
        }
        return chain.proceed(chain.request())
    }

    companion object {

        fun checkWifi(context: Context): Boolean {
            val wifiManager: WifiManager = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
            val state = wifiManager.connectionInfo.supplicantState.toString()
            if ( state.equals("COMPLETED")) {
                return true
            } else {
                return false
            }
        }
    }
}