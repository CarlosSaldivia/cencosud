package com.carlossaldivia.movie.component
import com.carlossaldivia.movie.base.App
import com.carlossaldivia.movie.module.AppModule
import com.carlossaldivia.movie.presentation.ui.fragments.HomeFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(app: App)
//    fun inject(homeFragment: HomeFragment)
}